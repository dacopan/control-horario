package ec.uce.controlhorario.service;

import ec.uce.controlhorario.dao.HorarioDao;
import ec.uce.controlhorario.dao.RegistroDao;
import ec.uce.controlhorario.dao.UsuarioDao;
import ec.uce.controlhorario.entidad.Asistencia;
import ec.uce.controlhorario.entidad.Horario;
import ec.uce.controlhorario.entidad.RegistroTimbre;
import ec.uce.controlhorario.entidad.Usuarios;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author dacopanCM on 15/03/18.
 */
@Service
public class ReporteService {

    private final Log log = LogFactory.getLog(getClass());

    private final HorarioDao horarioDao;
    private final RegistroDao registroDao;
    private final DataSource dataSource;
    private final UsuarioDao usuarioDao;

    public ReporteService(HorarioDao horarioDao, RegistroDao registroDao, DataSource dataSource, UsuarioDao usuarioDao) {
        this.horarioDao = horarioDao;
        this.registroDao = registroDao;
        this.dataSource = dataSource;
        this.usuarioDao = usuarioDao;
    }

    public List<Asistencia> generarAsistencia(Integer periodoId, Integer usuarioId) {
        List<Asistencia> result = new ArrayList<>();

        List<Horario> horasClase = horarioDao.findAllByPeriodoIdAndUsuariosId(periodoId, usuarioId);
        if (horasClase.size() < 1) {
            return result;
        }
        Horario horario = horasClase.get(0);
        List<RegistroTimbre> registro = registroDao.findAllByUsuarioIdAndFechaBetween(usuarioId, horario.getPeriodo().getInicio(), horario.getPeriodo().getFin());

        //.withDayOfMonth(1);
        //LocalDate end = initial.withDayOfMonth(initial.lengthOfMonth());

        LocalDate start = horario.getPeriodo().getInicio().toLocalDate();
        LocalDate end = horario.getPeriodo().getFin().toLocalDate();

        // recorremos dia a dia, los dias del semestre
        for (LocalDate date = start; date.isBefore(end); date = date.plusDays(1)) {
            System.out.println("procesando fecha: " + date);

            LocalDate finalDate = date;
            // de cada dia tiene varias horasClase entonces recorrermos todas las horas clase del dia en curso
            horasClase.stream().filter(h -> h.getDia() == finalDate.getDayOfWeek().getValue()).forEach(h -> {

                Asistencia asistencia = new Asistencia();
                asistencia.setActividad(h.getActividad().getNombre());
                asistencia.setDate(finalDate);
                asistencia.setMinutosAtrazo(-1);
                asistencia.setMinutosSalida(-1);
                asistencia.setMes(finalDate.getMonthValue());

                // por cada hora clase comprobamos si existe el registro de entrada y salida
                registro.stream().filter(r -> r.getFecha().toLocalDateTime().toLocalDate().isEqual(finalDate)).forEach(r -> {
                    LocalTime timeReg = r.getFecha().toLocalDateTime().toLocalTime();

                    // ChronoUnit.MINUTES.between(d1,d2)
                    // positivo si d2>d1
                    long minutosEntrada = ChronoUnit.MINUTES.between(timeReg, h.getHoraInicio().toLocalTime());
                    long minutosSalida = ChronoUnit.MINUTES.between(timeReg, h.getHoraFin().toLocalTime());
                    long minutosEntradaAbs = Math.abs(minutosEntrada);
                    long minutosSalidaAbs = Math.abs(minutosSalida);

                    if (minutosEntradaAbs <= TOLERANCIA_NORMAL) {// entrada normal
                        asistencia.setMinutosAtrazo(0);
                        asistencia.setIngreso(timeReg);
                    } else if (minutosEntrada <= 0 && minutosEntradaAbs <= TOLERANCIA_ATRAZO) {//entrada anticipada
                        asistencia.setMinutosAtrazo(0);
                        asistencia.setIngreso(timeReg);
                    } else if (minutosEntradaAbs <= TOLERANCIA_ATRAZO) { // atrazo
                        asistencia.setMinutosAtrazo(minutosEntradaAbs);
                        asistencia.setIngreso(timeReg);
                    }

                    if (minutosSalidaAbs <= TOLERANCIA_NORMAL) {// salida normal
                        asistencia.setMinutosSalida(0);
                        asistencia.setSalida(timeReg);
                    } else if (minutosSalida >= 0 && minutosSalidaAbs <= TOLERANCIA_ATRAZO) {// salida pasada la hora
                        asistencia.setMinutosSalida(0);
                        asistencia.setSalida(timeReg);
                    } else if (minutosSalidaAbs <= TOLERANCIA_ATRAZO) {// salida anticipada
                        asistencia.setMinutosSalida(minutosSalidaAbs);
                        asistencia.setSalida(timeReg);
                    }

                });

                if (asistencia.getMinutosAtrazo() < 0) {
                    asistencia.setEstado(ESTADO_FALTA);
                    asistencia.setEstadoNum(3);
                } else if (asistencia.getMinutosAtrazo() == 0 && asistencia.getMinutosSalida() == 0) {
                    asistencia.setEstado(ESTADO_NORMAL);
                    asistencia.setEstadoNum(0);
                } else if (asistencia.getMinutosAtrazo() > 0) {
                    asistencia.setEstado(ESTADO_ATRAZO);
                    asistencia.setEstadoNum(1);
                } else {
                    asistencia.setEstado(ESTADO_SALIDA_ANTICIPADA);
                    asistencia.setEstadoNum(2);
                }

                result.add(asistencia);

            });

        }

        result.forEach(r -> {
            if (r.getMinutosAtrazo() < 1) {
                r.setMinutosAtrazo(0);
            }
            if (r.getMinutosSalida() < 1) {
                r.setMinutosSalida(0);
            }
        });
        return result;
    }

    int TOLERANCIA_NORMAL = 10;
    int TOLERANCIA_ATRAZO = 15;
    String ESTADO_FALTA = "Falta";
    String ESTADO_NORMAL = "Normal";
    String ESTADO_ATRAZO = "Atrazo";
    String ESTADO_SALIDA_ANTICIPADA = "Salida Anticipada";


    /**
     * inicilaiza el JasperPrint para generar el reporte
     *
     * @param reportName nombre del reporte a generar relativo a {@literal /src/main/resources/report}
     * @param params     parametros del reporte
     * @param dataSource origen de datos del reporte
     * @return JasperPrint
     * @throws JRException si algun error
     */
    public JasperPrint init(String reportName, Map<String, Object> params, JRDataSource dataSource) throws JRException {
        //params.put("logo", config.isReportLogo());
        InputStream stream = this.getClass().getClassLoader().getResourceAsStream("" + reportName);
        return JasperFillManager.fillReport(stream, params, dataSource);
    }

    /**
     * inicilaiza el JasperPrint para generar el reporte
     *
     * @param reportName nombre del reporte a generar relativo a {@literal /src/main/resources/report}
     * @param params     parametros del reporte
     * @return JasperPrint
     * @throws JRException si algun error
     */
    public JasperPrint init(String reportName, Map<String, Object> params) throws JRException {
        JasperPrint print = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            InputStream stream = this.getClass().getClassLoader().getResourceAsStream("report/" + reportName);
            print = JasperFillManager.fillReport(stream, params, connection);

        } catch (Exception ex) {
            // ignorar error al generar reporte
            log.error("error al llenar reporte", ex);
        } finally {
            try {
                connection.close(); // cerramos la conexion
            } catch (SQLException ignored) {
                //ignorar no se requiere hacer nada aqui
            }
        }
        return print;
    }


    public ByteArrayOutputStream downloadPdf(JasperPrint jasperPrint, String filename) throws JRException, IOException {
        if (jasperPrint == null) {
            throw new RuntimeException("Error al generar reporte");
        }
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        JasperExportManager.exportReportToPdfStream(jasperPrint, os);
        os.flush();
        os.close();
        return os;
    }


    public ByteArrayOutputStream openExcel(JasperPrint jasperPrint, String filename) throws JRException, IOException {
        if (jasperPrint == null) {
            throw new RuntimeException("Error al generar reporte");
        }
        ByteArrayOutputStream os = new ByteArrayOutputStream();


        JRXlsxExporter exporter = new JRXlsxExporter();
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(os));

        SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
        configuration.setOnePagePerSheet(true);
        configuration.setDetectCellType(true);
        configuration.setCollapseRowSpan(false);
        exporter.setConfiguration(configuration);
        exporter.exportReport();

        os.flush();
        os.close();
        return os;
    }

    public Usuarios findUser(Integer usuarioId) {
        return usuarioDao.findById(usuarioId).orElse(null);
    }
}
